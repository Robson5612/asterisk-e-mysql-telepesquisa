# ASTERISK e MYSQL - Telepesquisa

Essa aplicação tem o intuito de proporcionar ao cliente a realizar uma avaliação ao atendimento de um operador.

Após realizar um atendimento o operador deve transferir a ligação para o número 350 que é uma URA, deve ser reproduzido um áudio ao cliente e o mesmo deve avaliar o atendimento do operador com uma nota de 0 a 10

Para utilização desta aplicação basta seguir os passos abaixo:

1- Criar um banco de dados no MYSQL com o nome de "pesquisa" e importar a tabela atendimento que se encontra na pasta Telepesquisa;

2- Substituir o arquivo extensions_custom.conf original do seu server pelo presente nesse aquivo, preencher os campos de usuário e senha da conexão com MYSQL e também preencher o nome do contexto usado nos ramais; 

3- Caso queira utilizar a URA em texto basta descomentar as primeiras linhas do arquivo extensions_custom.conf 

OBS: o áudio da que será executado na URA deve ser confeccionado pela propria empresa, o que garante mais exclusividade durante a utilização da aplicação.
