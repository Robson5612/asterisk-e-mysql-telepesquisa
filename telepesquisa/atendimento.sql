-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 23/03/2021 às 11:19
-- Versão do servidor: 5.5.62-0+deb8u1
-- Versão do PHP: 5.6.40-0+deb8u12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `pesquisa`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `atendimento`
--

CREATE TABLE IF NOT EXISTS `atendimento` (
`ID` int(10) NOT NULL,
  `cliente` text NOT NULL,
  `ramal` text NOT NULL,
  `data` varchar(10) NOT NULL,
  `horario` varchar(10) CHARACTER SET utf8 NOT NULL,
  `nota` int(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `atendimento`
--

-- INSERT INTO `atendimento` (`ID`, `cliente`, `ramal`, `ramalnovo`, `data`, `horario`, `nota`) VALUES


-- Índices de tabelas apagadas
--

--
-- Índices de tabela `atendimento`
--
ALTER TABLE `atendimento`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `atendimento`
--
ALTER TABLE `atendimento`
MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
